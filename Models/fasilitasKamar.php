<?php

    require_once("../Config/connection.php");


    Class fasilitasKamar
    {
        public function __construct()
        {
            
        }

        public function get_data()
        {
            $sql = "SELECT tb_fasilitas.id_fasilitaskamar,
                    tb_fasilitas.tipe_kamar,
                    tb_fasilitas.fasilitas_kamar,
                    tb_kamar.tipe_kamar
                    FROM tb_fasilitas INNER JOIN tb_kamar ON
                    tb_fasilitas.tipe_kamar = tb_kamar.id_kamar";
            return runQuery($sql);
        }
        public function insert($tipe_kamar, $fasilitas_kamar)
        {
            $sql = "INSERT INTO tb_fasilitas (tipe_kamar, fasilitas_kamar) VALUES ('$tipe_kamar', '$fasilitas_kamar')";
            return runQuery($sql);
        }

        public function update($id_fasilitaskamar, $tipe_kamar, $fasilitas_kamar)
        {
            $sql = "UPDATE tb_fasilitas SET tipe_kamar='$tipe_kamar', fasilitas_kamar='$fasilitas_kamar'
                    WHERE id_fasilitaskamar='$id_fasilitaskamar'";
            return runQuery($sql);
        }

        public function show($id_fasilitaskamar)
        {
            $sql = "SELECT * FROM tb_fasilitas WHERE id_fasilitaskamar='$id_fasilitaskamar'";
            return runQueryRow($sql);
        }

        public function delete_data($id_fasilitaskamar)
        {
            $sql = "DELETE FROM tb_fasilitas WHERE id_fasilitaskamar='$id_fasilitaskamar'";
            return runQuery($sql);
        }

    }