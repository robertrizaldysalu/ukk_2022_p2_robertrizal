<?php

    require_once("../Config/connection.php");


    Class fasilitasUmum
    {
        public function __construct()
        {}

        public function get_data()
        {
            $sql = "SELECT * FROM tb_fasilitas_umum";
            return runQuery($sql);
        }

        public function insert($nm_fasilitas, $keterangan)
        {
            $sql = "INSERT INTO tb_fasilitas_umum (nm_fasilitas, keterangan) VALUES ('$nm_fasilitas', '$keterangan')";
            return runQuery($sql);
        }

        public function update($id_fasilitas, $nm_fasilitas, $keterangan)
        {
            $sql = "UPDATE tb_fasilitas_umum SET nm_fasilitas='$nm_fasilitas', keterangan='$keterangan'
                    WHERE id_fasilitas='$id_fasilitas'";
            return runQuery($sql);
        }

        public function show($id_fasilitas)
        {
            $sql = "SELECT * FROM tb_fasilitas_umum WHERE id_fasilitas='$id_fasilitas'";
            return runQueryRow($sql);
        }

        public function delete_data($id_fasilitas)
        {
            $sql = "DELETE FROM tb_fasilitas_umum WHERE id_fasilitas='$id_fasilitas'";
            return runQuery($sql);
        }

    }