-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2022 at 01:09 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ukk_22_p2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_fasilitas`
--

CREATE TABLE `tb_fasilitas` (
  `id_fasilitaskamar` int(11) NOT NULL,
  `tipe_kamar` varchar(50) NOT NULL,
  `fasilitas_kamar` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_fasilitas`
--

INSERT INTO `tb_fasilitas` (`id_fasilitaskamar`, `tipe_kamar`, `fasilitas_kamar`) VALUES
(15, 'Deluxe', 'sas'),
(30, '1', 'kasur'),
(31, '2', 'kasur'),
(32, '2', 'kasur'),
(33, '', 'kasur'),
(34, '', 'kasur'),
(35, '', 'kasur,ac'),
(36, '', 'kasur'),
(37, '', 'ac tiv'),
(38, '1', 'kasur');

-- --------------------------------------------------------

--
-- Table structure for table `tb_fasilitas_umum`
--

CREATE TABLE `tb_fasilitas_umum` (
  `id_fasilitas` int(11) NOT NULL,
  `nm_fasilitas` varchar(100) NOT NULL,
  `keterangan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_fasilitas_umum`
--

INSERT INTO `tb_fasilitas_umum` (`id_fasilitas`, `nm_fasilitas`, `keterangan`) VALUES
(1, 'kolam', '32m1');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kamar`
--

CREATE TABLE `tb_kamar` (
  `id_kamar` int(11) NOT NULL,
  `tipe_kamar` varchar(50) NOT NULL,
  `jml` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kamar`
--

INSERT INTO `tb_kamar` (`id_kamar`, `tipe_kamar`, `jml`) VALUES
(1, 'Deluxe', 32),
(2, 'Superior', 32),
(3, 'vip', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `id_pesanan` int(11) NOT NULL,
  `nm_pemesanan` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `nm_tamu` varchar(150) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  `cek_in` date NOT NULL,
  `cek_out` date NOT NULL,
  `jml` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_pemesanan`
--

INSERT INTO `tb_pemesanan` (`id_pesanan`, `nm_pemesanan`, `email`, `no_hp`, `nm_tamu`, `id_kamar`, `cek_in`, `cek_out`, `jml`) VALUES
(1, 'indra', 'robertrizaldysalu@gmail.com', '08595966', 'dani', 1, '2022-05-09', '2022-05-09', 1),
(2, 'indra', 'robertrizaldysalu@gmail.com', '08595966', 'dani', 2, '2022-05-09', '2022-05-09', 1),
(3, 'indra', 'robertrizaldysalu@gmail.com', '08595966', 'dani', 1, '2022-05-09', '2022-05-09', 1),
(4, 'robert', 'robert@gmail.com', '0938337373', 'robert', 2, '2022-05-11', '2022-05-26', 1),
(33, 'riki', 'robertrizaldysalu@gmail.com', '08595966', 'gerung', 3, '2022-05-11', '2022-05-11', 127),
(34, 'Ricky ', 'ricky@gmail.com', '0895342671170', 'ricky', 2, '2022-05-11', '2022-05-12', 1),
(35, 'indra', 'robertrizaldysalu@gmail.com', '08595966', 'dani', 1, '2022-05-11', '2022-05-20', 35),
(36, 'ricky', 'robertrizaldysalu@gmail.com', '0938383', 'ds', 1, '2022-05-26', '2022-06-02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `role` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `pass`, `role`) VALUES
(1, 'admin', '123', 1),
(2, 'resepsionis', '123', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_fasilitas`
--
ALTER TABLE `tb_fasilitas`
  ADD PRIMARY KEY (`id_fasilitaskamar`);

--
-- Indexes for table `tb_fasilitas_umum`
--
ALTER TABLE `tb_fasilitas_umum`
  ADD PRIMARY KEY (`id_fasilitas`);

--
-- Indexes for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD PRIMARY KEY (`id_kamar`);

--
-- Indexes for table `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_fasilitas`
--
ALTER TABLE `tb_fasilitas`
  MODIFY `id_fasilitaskamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tb_fasilitas_umum`
--
ALTER TABLE `tb_fasilitas_umum`
  MODIFY `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tb_kamar`
--
ALTER TABLE `tb_kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  MODIFY `id_pesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
