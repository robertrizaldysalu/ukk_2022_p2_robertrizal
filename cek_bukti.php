<?php
   require_once("Config/connection.php");
   session_start();
   if(isset($_SESSION["nm_pemesanan"])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Top Navigation</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="public/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="public/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="public/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/custom/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="public/custom/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="public/index2.html" class="navbar-brand"><b>Admin</b>LTE</a>
        </div>

        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
   
      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="margin-top: 50%">
            <div class="box-header">
              <h3 class="box-title">Cek Bukti Reservasi</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                <th>Nama Pemesanan</th>
                <th>Email</th>
                <th>No HP</th>
                <th>Nama Tamu</th>
                <th>Cek In</th>
                <th>Cek Out</th>
                <th>Jumlah Kamar</th>
              </tr>
              <tr>
                <td><?php echo $_SESSION["nm_pemesanan"] ?></td>
                <td><?php echo $_SESSION["email"] ?></td>
                <td><?php echo $_SESSION["no_hp"] ?></td>
                <td><?php echo $_SESSION["nm_tamu"] ?></td>
                <td><?php echo $_SESSION["cek_in"] ?></td>
                <td><?php echo $_SESSION["cek_out"] ?></td>
                <td><?php echo $_SESSION["jml"] ?></td>
              </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="public/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="public/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="public/custom/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="public/custom/js/demo.js"></script>

<script>
    window.print();
</script>
</body>
</html>


<?php
session_destroy();
   }else{
       header("Location:".BASE_URL);
   }
   ?>