<?php
    require_once("../Models/fasilitasUmum.php");

    $fasilitasUmum = new fasilitasUmum();

    // Cek jika ada id_kamar,nm_fasilitas,keterangan pada req
    // Jika ada jalankan metode cleanString
    // Jika tidak ada maka kosongkan
    $id_fasilitas = isset($_POST["id_fasilitas"]) ? cleanString($_POST["id_fasilitas"]): "";
    $nm_fasilitas = isset($_POST["nm_fasilitas"]) ? cleanString($_POST["nm_fasilitas"]): "";
    $keterangan = isset($_POST["keterangan"]) ? cleanString($_POST["keterangan"]): "";

    // Struktur Kendali CRUD
    switch ($_GET["action"]){
        case 'saveOrEdit' :
            if(empty($id_fasilitas)){
                // Jika id_fasilitas tidak ada pada req, jalankan method insert
                $response = $fasilitasUmum->insert($nm_fasilitas, $keterangan);
            } else {
                // Jika id_fasilitas ada, jalankan method edit
                $response = $fasilitasUmum->update($id_fasilitas, $nm_fasilitas, $keterangan);
            }
        break;

        case 'get_data' :
            $response = $fasilitasUmum->get_data();

            $data = Array();

            while($row = $response->fetch_object()){
                $data[] = array(
                    "0"=>$row->nm_fasilitas,
                    "1"=>$row->keterangan,
                    "2"=>'<button class="btn btn-info btn-sm" onclick="show('.$row->id_fasilitas.')" title="Edit Data"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm" onclick="delete_data('.$row->id_fasilitas.')" title="Delete Data"><i class="fa fa-trash"></i></button>'
                );
            }

            $result = array(
                "sEcho"=>1,
                "iTotalRecords"=>count($data),
                "aaData"=>$data
            );

            echo json_encode($result);
        break;
        case 'show' :
            $response = $fasilitasUmum->show($id_fasilitas);
            echo json_encode($response);
        break;
        case 'delete_data' :
            $response = $fasilitasUmum->delete_data($id_fasilitas);
        break;
    }