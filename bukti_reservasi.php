<!DOCTYPE html>
<html lang="en">
<head>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="Public/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="Public/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="Public/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="Public/custom/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="Public/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="Home/index.html"><b>Cetak</b>Reservasi</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <!-- Notification -->
    <?php
      require 'view/layoutPartial/alert.php';
    ?>
    <!-- END NOTIFICATIONS -->
    <form action="" id="login_form" method="post">
    <!-- Role ID 1 = Admin -->
 

      <div class="form-group has-feedback">
        <input type="text" id="user_nickname" name="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <input type="hidden" name="submit" value="login">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Cetak</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="Public/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="Public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="Public/plugins/iCheck/icheck.min.js"></script>

</body>
</html>
