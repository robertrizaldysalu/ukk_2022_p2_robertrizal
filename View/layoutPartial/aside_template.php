<?php
   if($_SESSION["id_user"]==1){
	   ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../kamar/kamar.php">
            <i class="fa fa-plane"></i> <span>Data Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fasilitas/fasilitasKamar.php">
            <i class="fa fa-cab"></i> <span>Fasilitas Kamar</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../fasilitasUmum/fasilitasUmum.php">
            <i class="fa fa-subway"></i> <span>Fasilitas umum</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../logout.php">
            <i class="fa fa-sign-out"></i> <span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<?php
   }else{
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../DetailData/dataUser.php">
            <i class="fa fa-bed"></i> <span>Detail Data User</span>
          </a>
        </li>
      </ul>
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../logout.php">
          <i class="fa fa-sign-out"></i><span>Logout</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
</aside>

      <?php
   }

?>
